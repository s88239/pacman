# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first.
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"
    frontier = util.PriorityQueue()
    startNode = problem.getStartState()
    visited = [] # the queue for checking visited nodes
    nodeParent = {}
    uniformCost = { startNode:0 }
    frontier.push( startNode, 0 ) # push the root node to priority queue
    while not frontier.isEmpty():
        currentNode = frontier.pop()
        if problem.isGoalState( currentNode ): # found the goal
            return findPath( nodeParent, currentNode ) # return the path
        visited.append( currentNode )
        successorlist = problem.getSuccessors( currentNode )
        for successor in successorlist:
            newCost = uniformCost[currentNode] + successor[2]
            if visited.count( successor[0] ) == 0 :
                if not uniformCost.has_key( successor[0]):
                    frontier.push( successor[0], newCost )
                    uniformCost[ successor[0] ] = newCost
                    nodeParent[ successor[0] ] = (currentNode, successor[1]) # ( parentNode, direction )
                elif uniformCost.has_key( successor[0] ) and newCost < uniformCost[ successor[0] ] :
                    frontier = findAndReplacePriorityQueue( frontier, uniformCost, successor[0], newCost )
                    uniformCost[ successor[0] ] = newCost
                    nodeParent[ successor[0] ] = (currentNode, successor[1]) # ( parentNode, direction )
    util.raiseNotDefined()

def findAndReplacePriorityQueue( queue, priority, node, cost):
    newList = []
    while not queue.isEmpty():
        queueNode = queue.pop()
        if queueNode == node: # find the equal node in the frontier
            queue.push(node, cost) # replace it
            break
        newList.append(queueNode)

    for member in newList: # transfer found nodes to original priority queue
        queue.push( member, priority[ member ] )

    return queue

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"
    priorityQ  = util.PriorityQueue()
    startNode = problem.getStartState()
    visited = [] # the queue for checking visited nodes
    startNode_h = heuristic(startNode, problem)
    nodeParent = {}
    g_realCost = { startNode:0 } # g(n) of A* seach
    h_heuristicCost = { startNode:startNode_h } # h(n) of A* search
    priorityQ.push( startNode, startNode_h ) # push the root node to priority queue
    while not priorityQ.isEmpty():
        currentNode = priorityQ.pop()
        if problem.isGoalState( currentNode ): # found the goal
            return findPath( nodeParent, currentNode ) # return the path
        visited.append( currentNode )
        successorlist = problem.getSuccessors( currentNode )
        for successor in successorlist:
            sg = g_realCost[currentNode] + successor[2]
            sh = sg + heuristic( successor[0], problem )
            if visited.count( successor[0] ) > 0 and sh >= h_heuristicCost[ successor[0] ] :
                continue
            elif not h_heuristicCost.has_key( successor[0] ):
                nodeParent[ successor[0] ] = (currentNode, successor[1]) # ( parentNode, direction )
                g_realCost[ successor[0] ] = sg
                h_heuristicCost[ successor[0] ] = sh
                priorityQ.push( successor[0], sh )
            elif sh < h_heuristicCost[ successor[0] ]:
                priorityQ = findAndReplacePriorityQueue( priorityQ, h_heuristicCost, successor[0], sh )
                nodeParent[ successor[0] ] = (currentNode, successor[1]) # ( parentNode, direction )
                g_realCost[ successor[0] ] = sg
                h_heuristicCost[ successor[0] ] = sh
    util.raiseNotDefined()

def findPath( nodeParent, goal ): # find the path from start to goal
    if nodeParent.has_key( goal ):
        pathList = findPath( nodeParent, nodeParent[ goal ][0] )
        pathList.append( nodeParent[ goal ][1] )
        return pathList # return the path from parent plus itself
    else:
        return [] # start node, return empty list

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
